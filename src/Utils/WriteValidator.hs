{-# LANGUAGE TypeApplications #-}

module  Utils.WriteValidator
            (   writeCBORValidator
            ,   writeSpendingNormalOptimizedScript
            ,   writeSpendingAggressiveOptimizedScript
            ,   writeMintingNormalOptimizedScript
            ,   writeMintingAggressiveOptimizedScript
            ,   writeStakingNormalOptimizedScript
            ,   writeStakingAggressiveOptimizedScript
            ,   getCurrencySymbolByNormalOptimization
            ,   getCurrencySymbolByAggressiveOptimization
            ,   getValidatorHashByNormalOptimization
            ,   getValidatorHashByAggressiveOptimization
            ,   getStakeValidatorHashByNormalOptimization
            ,   getStakeValidatorHashByAggressiveOptimization

            )
                where

import           Cardano.Api.SerialiseTextEnvelope (TextEnvelopeDescr,
                                                    writeFileTextEnvelope)
import           Cardano.Api.Shelley               (FileError,
                                                    PlutusScript (PlutusScriptSerialised),
                                                    PlutusScriptV2,
                                                    displayError)
import           Codec.Serialise                   (serialise)
import           Data.ByteString.Lazy              (toStrict)
import qualified Data.ByteString.Lazy              as LBS
import           Data.ByteString.Short             (length, toShort)
import           Data.List                         (break, intercalate, reverse)
import           Data.List.Split                   (chunksOf)
import qualified Plutonomy                         (aggressiveOptimizerOptions,
                                                    optimizeUPLC,
                                                    optimizeUPLCWith)
import           Plutus.Script.Utils.V2.Scripts    (scriptCurrencySymbol,
                                                    scriptHash,
                                                    stakeValidatorHash,
                                                    validatorHash)
import           Plutus.V1.Ledger.Scripts          (StakeValidatorHash,
                                                    ValidatorHash, scriptSize,
                                                    unScript)
import           Plutus.V2.Ledger.Api              (CurrencySymbol, Data (..),
                                                    Script,
                                                    mkMintingPolicyScript,
                                                    mkStakeValidatorScript,
                                                    mkValidatorScript,
                                                    unMintingPolicyScript,
                                                    unStakeValidatorScript,
                                                    unValidatorScript)
import           PlutusCore.Pretty                 (prettyPlcReadableDef)
import           PlutusTx                          (BuiltinData, CompiledCode)
import           PlutusTx.Code                     (getPlc, sizePlc)
import           Prelude                           as Haskell (Bool (..), Char,
                                                               Either (..),
                                                               FilePath, IO,
                                                               Maybe (..), Show,
                                                               fst, print,
                                                               putStrLn, show,
                                                               snd, writeFile,
                                                               ($), (++), (.),
                                                               (<>), (==))
import           System.Directory                  (createDirectoryIfMissing)
import           System.FilePath.Posix             ((<.>), (</>))
-- ##############  Utils ############# --

import           Utils.ExBudget                    (getCPUExBudget,
                                                    getMemoryExBudget,
                                                    plutusCore)
import           Utils.ToJSON                      (writeOnlyJSON)

getCurrencySymbolByNormalOptimization :: CompiledCode (BuiltinData -> BuiltinData -> ()) -> CurrencySymbol
getCurrencySymbolByNormalOptimization script =
    scriptCurrencySymbol $ Plutonomy.optimizeUPLC $ mkMintingPolicyScript script

getCurrencySymbolByAggressiveOptimization :: CompiledCode (BuiltinData -> BuiltinData -> ()) -> CurrencySymbol
getCurrencySymbolByAggressiveOptimization script =
    scriptCurrencySymbol $ Plutonomy.optimizeUPLCWith Plutonomy.aggressiveOptimizerOptions $ mkMintingPolicyScript script

getValidatorHashByNormalOptimization :: CompiledCode (BuiltinData -> BuiltinData -> BuiltinData-> ()) -> ValidatorHash
getValidatorHashByNormalOptimization script =
    validatorHash $ Plutonomy.optimizeUPLC $ mkValidatorScript script

getValidatorHashByAggressiveOptimization :: CompiledCode (BuiltinData -> BuiltinData -> BuiltinData-> ()) -> ValidatorHash
getValidatorHashByAggressiveOptimization script =
    validatorHash $ Plutonomy.optimizeUPLCWith Plutonomy.aggressiveOptimizerOptions $ mkValidatorScript script

getStakeValidatorHashByNormalOptimization :: CompiledCode (BuiltinData -> BuiltinData-> ()) -> StakeValidatorHash
getStakeValidatorHashByNormalOptimization script =
    stakeValidatorHash $ Plutonomy.optimizeUPLC $ mkStakeValidatorScript script

getStakeValidatorHashByAggressiveOptimization :: CompiledCode (BuiltinData -> BuiltinData-> ()) -> StakeValidatorHash
getStakeValidatorHashByAggressiveOptimization script =
    stakeValidatorHash $ Plutonomy.optimizeUPLCWith Plutonomy.aggressiveOptimizerOptions $ mkStakeValidatorScript script

format :: Show a => a -> [Char]
format x = h++t
    where
        sp = break (== '.') $ show x
        h = reverse (intercalate "," $ chunksOf 3 $ reverse $ fst sp)
        t = snd sp


contractDescription :: TextEnvelopeDescr
contractDescription  = "..............."

writeCBORValidator :: FilePath -> Script -> IO (Either (FileError ()) ())
writeCBORValidator file = writeFileTextEnvelope @(PlutusScript PlutusScriptV2) file (Just contractDescription) . PlutusScriptSerialised . toShort . toStrict . serialise

writeSpendingNormalOptimizedScript  :: CompiledCode (BuiltinData -> BuiltinData -> BuiltinData -> ()) -> [Data] -> [Char] -> [Char] -> [Char] -> IO ()
writeSpendingNormalOptimizedScript  cC d contractName conDir coresDir = do
    let validator       = Plutonomy.optimizeUPLC $ mkValidatorScript cC
        plc             = getPlc cC
        --pir             = getPir cC
        plcSize         = sizePlc cC
        contractScript  = unValidatorScript validator
    createDirectoryIfMissing True conDir
    createDirectoryIfMissing True $ coresDir </> contractName
    result <- writeCBORValidator (conDir </> contractName <.> ".plutus") contractScript
    case result of
        Left err -> print $ displayError err
        Right () -> do
                        Haskell.writeFile  (coresDir </> contractName </> "prettifiedTypedPlutusCore" <.> "txt") (show $ prettyPlcReadableDef plc)
                        Haskell.writeFile  (coresDir </> contractName </> "rawTypePlutusCore" <.> "txt") (show plc)
                        Haskell.writeFile  (coresDir </> contractName </> "prettifiedUntypedPlutusCore" <.> "txt") (show (prettyPlcReadableDef (unScript contractScript)))
                        Haskell.writeFile  (coresDir </> contractName </> "rawUntypedPlutusCore" <.> "txt") (show (unScript contractScript))
                        --writeFile  (coresDir </> contractName </> contractName <.> ".PIR") (show $ prettyPIR agkp)

                        Haskell.putStrLn        "\n<----------------------------------------CONTRACT INFO----------------------------------------->\n"
                        Haskell.putStrLn    $   "       Name:                       "   ++  contractName
                        Haskell.putStrLn    $   "       Location:                   "   ++  conDir      </> contractName
                        Haskell.putStrLn    $   "       Cores Location:             "   ++  coresDir    </> contractName
                        Haskell.putStrLn    $   "       PLC Size (Bytes):           "   <>  show (format plcSize)
                        Haskell.putStrLn    $   "       CBOR Size (Bytes):          "   <>  show (format $  LBS.length $ writeOnlyJSON contractScript)
                        Haskell.putStrLn    $   "       CBOR Binary Size (Bytes):   "   <>  show (format $  length (plutusCore contractScript))
                        Haskell.putStrLn    $   "       Plutus Binary Size (Bytes): "   <>  show (format $  scriptSize contractScript)
                        Haskell.putStrLn    $   "       CPU Usage (Step/PSec):      "   <>  show (format $  getCPUExBudget d contractScript)
                        Haskell.putStrLn    $   "       Memory Usage (Bytes):       "   <>  show (format $  getMemoryExBudget d contractScript)
                        Haskell.putStrLn    $   "       Script Hash:                "   <>  show (scriptHash contractScript)
                        Haskell.putStrLn        "\n<---------------------------------------------------------------------------------------------->\n"

                        --putStrLn        "\n<---------------------------------UNTYPED PLUTUS CORE---------------------------------->\n"
                        --print       $       prettyPlcReadableDef uplc
                        --putStrLn          "\n<-------------------------------------------------------------------------------------->\n"
--
                        --putStrLn        "\n<----------------------------------TYPED PLUTUS CORE----------------------------------->\n"
                        --print       $       prettyPlcReadableDef $  contractParams
                        --putStrLn          "\n<-------------------------------------------------------------------------------------->\n"
--
                        --putStrLn        "\n<----------------------------------------PLUTUS IR------------------------------------->\n"
                        --print       $       pretty $ fromJust (Just (pir contractParams))
                        --putStrLn          "\n<-------------------------------------------------------------------------------------->\n"

        -- prettyPIR :: ContractParams -> Doc ann0
        -- prettyPIR cp = pretty $ fromMaybe (error "ERROR: No Plutus IR") (pir cp)

writeMintingNormalOptimizedScript  :: CompiledCode (BuiltinData -> BuiltinData -> ()) -> [Data] -> [Char] -> [Char] -> [Char] -> IO ()
writeMintingNormalOptimizedScript  cC d contractName conDir coresDir = do
    let validator       =  Plutonomy.optimizeUPLC $ mkMintingPolicyScript cC
        plc             = getPlc cC
        --pir             = getPir cC
        plcSize         = sizePlc cC
        contractScript  = unMintingPolicyScript validator
    createDirectoryIfMissing True conDir
    createDirectoryIfMissing True $ coresDir </> contractName
    result <- writeCBORValidator (conDir </> contractName <.> ".plutus") contractScript
    case result of
        Left err -> print $ displayError err
        Right () -> do
                        Haskell.writeFile  (coresDir </> contractName </> "prettifiedTypedPlutusCore" <.> "txt") (show $ prettyPlcReadableDef plc)
                        Haskell.writeFile  (coresDir </> contractName </> "rawTypePlutusCore" <.> "txt") (show plc)
                        Haskell.writeFile  (coresDir </> contractName </> "prettifiedUntypedPlutusCore" <.> "txt") (show (prettyPlcReadableDef (unScript contractScript)))
                        Haskell.writeFile  (coresDir </> contractName </> "rawUntypedPlutusCore" <.> "txt") (show (unScript contractScript))
                        --writeFile  (coresDir </> contractName </> contractName <.> ".PIR") (show $ prettyPIR agkp)

                        Haskell.putStrLn        "\n<----------------------------------------CONTRACT INFO----------------------------------------->\n"
                        Haskell.putStrLn    $   "       Name:                       "   ++  contractName
                        Haskell.putStrLn    $   "       Location:                   "   ++  conDir      </> contractName
                        Haskell.putStrLn    $   "       Cores Location:             "   ++  coresDir    </> contractName
                        Haskell.putStrLn    $   "       PLC Size (Bytes):           "   <>  show (format plcSize)
                        Haskell.putStrLn    $   "       CBOR Size (Bytes):          "   <>  show (format $  LBS.length $ writeOnlyJSON contractScript)
                        Haskell.putStrLn    $   "       CBOR Binary Size (Bytes):   "   <>  show (format $  length (plutusCore contractScript))
                        Haskell.putStrLn    $   "       Plutus Binary Size (Bytes): "   <>  show (format $  scriptSize contractScript)
                        Haskell.putStrLn    $   "       CPU Usage (Step/PSec):      "   <>  show (format $  getCPUExBudget d contractScript)
                        Haskell.putStrLn    $   "       Memory Usage (Bytes):       "   <>  show (format $  getMemoryExBudget d contractScript)
                        Haskell.putStrLn    $   "       Script Hash:                "   <>  show (scriptHash contractScript)
                        Haskell.putStrLn        "\n<---------------------------------------------------------------------------------------------->\n"

                        --putStrLn        "\n<---------------------------------UNTYPED PLUTUS CORE---------------------------------->\n"
                        --print       $       prettyPlcReadableDef uplc
                        --putStrLn          "\n<-------------------------------------------------------------------------------------->\n"
--
                        --putStrLn        "\n<----------------------------------TYPED PLUTUS CORE----------------------------------->\n"
                        --print       $       prettyPlcReadableDef $  contractParams
                        --putStrLn          "\n<-------------------------------------------------------------------------------------->\n"
--
                        --putStrLn        "\n<----------------------------------------PLUTUS IR------------------------------------->\n"
                        --print       $       pretty $ fromJust (Just (pir contractParams))
                        --putStrLn          "\n<-------------------------------------------------------------------------------------->\n"

        -- prettyPIR :: ContractParams -> Doc ann0
        -- prettyPIR cp = pretty $ fromMaybe (error "ERROR: No Plutus IR") (pir cp)

writeStakingNormalOptimizedScript  :: CompiledCode (BuiltinData -> BuiltinData -> ()) -> [Data] -> [Char] -> [Char] -> [Char] -> IO ()
writeStakingNormalOptimizedScript  cC d contractName conDir coresDir = do
    let validator       = Plutonomy.optimizeUPLC $ mkStakeValidatorScript cC
        plc             = getPlc cC
        --pir             = getPir cC
        plcSize         = sizePlc cC
        contractScript  = unStakeValidatorScript validator
    createDirectoryIfMissing True conDir
    createDirectoryIfMissing True $ coresDir </> contractName
    result <- writeCBORValidator (conDir </> contractName <.> ".plutus") contractScript
    case result of
        Left err -> print $ displayError err
        Right () -> do
                        Haskell.writeFile  (coresDir </> contractName </> "prettifiedTypedPlutusCore" <.> "txt") (show $ prettyPlcReadableDef plc)
                        Haskell.writeFile  (coresDir </> contractName </> "rawTypePlutusCore" <.> "txt") (show plc)
                        Haskell.writeFile  (coresDir </> contractName </> "prettifiedUntypedPlutusCore" <.> "txt") (show (prettyPlcReadableDef (unScript contractScript)))
                        Haskell.writeFile  (coresDir </> contractName </> "rawUntypedPlutusCore" <.> "txt") (show (unScript contractScript))
                        --writeFile  (coresDir </> contractName </> contractName <.> ".PIR") (show $ prettyPIR agkp)

                        Haskell.putStrLn        "\n<----------------------------------------CONTRACT INFO----------------------------------------->\n"
                        Haskell.putStrLn    $   "       Name:                       "   ++  contractName
                        Haskell.putStrLn    $   "       Location:                   "   ++  conDir      </> contractName
                        Haskell.putStrLn    $   "       Cores Location:             "   ++  coresDir    </> contractName
                        Haskell.putStrLn    $   "       PLC Size (Bytes):           "   <>  show (format plcSize)
                        Haskell.putStrLn    $   "       CBOR Size (Bytes):          "   <>  show (format $  LBS.length $ writeOnlyJSON contractScript)
                        Haskell.putStrLn    $   "       CBOR Binary Size (Bytes):   "   <>  show (format $  length (plutusCore contractScript))
                        Haskell.putStrLn    $   "       Plutus Binary Size (Bytes): "   <>  show (format $  scriptSize contractScript)
                        Haskell.putStrLn    $   "       CPU Usage (Step/PSec):      "   <>  show (format $  getCPUExBudget d contractScript)
                        Haskell.putStrLn    $   "       Memory Usage (Bytes):       "   <>  show (format $  getMemoryExBudget d contractScript)
                        Haskell.putStrLn    $   "       Script Hash:                "   <>  show (scriptHash contractScript)
                        Haskell.putStrLn        "\n<---------------------------------------------------------------------------------------------->\n"

                        --putStrLn        "\n<---------------------------------UNTYPED PLUTUS CORE---------------------------------->\n"
                        --print       $       prettyPlcReadableDef uplc
                        --putStrLn          "\n<-------------------------------------------------------------------------------------->\n"
--
                        --putStrLn        "\n<----------------------------------TYPED PLUTUS CORE----------------------------------->\n"
                        --print       $       prettyPlcReadableDef $  contractParams
                        --putStrLn          "\n<-------------------------------------------------------------------------------------->\n"
--
                        --putStrLn        "\n<----------------------------------------PLUTUS IR------------------------------------->\n"
                        --print       $       pretty $ fromJust (Just (pir contractParams))
                        --putStrLn          "\n<-------------------------------------------------------------------------------------->\n"

        -- prettyPIR :: ContractParams -> Doc ann0
        -- prettyPIR cp = pretty $ fromMaybe (error "ERROR: No Plutus IR") (pir cp)



writeSpendingAggressiveOptimizedScript  :: CompiledCode (BuiltinData -> BuiltinData -> BuiltinData -> ()) -> [Data] -> [Char] -> [Char] -> [Char] -> IO ()
writeSpendingAggressiveOptimizedScript  cC d contractName conDir coresDir = do
    let validator       = Plutonomy.optimizeUPLCWith Plutonomy.aggressiveOptimizerOptions $ mkValidatorScript cC
        plc             = getPlc cC
        --pir             = getPir cC
        plcSize         = sizePlc cC
        contractScript  = unValidatorScript validator
    createDirectoryIfMissing True conDir
    createDirectoryIfMissing True $ coresDir </> contractName
    result <- writeCBORValidator (conDir </> contractName <.> ".plutus") contractScript
    case result of
        Left err -> print $ displayError err
        Right () -> do
                        Haskell.writeFile  (coresDir </> contractName </> "prettifiedTypedPlutusCore" <.> "txt") (show $ prettyPlcReadableDef plc)
                        Haskell.writeFile  (coresDir </> contractName </> "rawTypePlutusCore" <.> "txt") (show plc)
                        Haskell.writeFile  (coresDir </> contractName </> "prettifiedUntypedPlutusCore" <.> "txt") (show (prettyPlcReadableDef (unScript contractScript)))
                        Haskell.writeFile  (coresDir </> contractName </> "rawUntypedPlutusCore" <.> "txt") (show (unScript contractScript))
                        --writeFile  (coresDir </> contractName </> contractName <.> ".PIR") (show $ prettyPIR agkp)

                        Haskell.putStrLn        "\n<----------------------------------------CONTRACT INFO----------------------------------------->\n"
                        Haskell.putStrLn    $   "       Name:                       "   ++  contractName
                        Haskell.putStrLn    $   "       Location:                   "   ++  conDir      </> contractName
                        Haskell.putStrLn    $   "       Cores Location:             "   ++  coresDir    </> contractName
                        Haskell.putStrLn    $   "       PLC Size (Bytes):           "   <>  show (format plcSize)
                        Haskell.putStrLn    $   "       CBOR Size (Bytes):          "   <>  show (format $  LBS.length $ writeOnlyJSON contractScript)
                        Haskell.putStrLn    $   "       CBOR Binary Size (Bytes):   "   <>  show (format $  length (plutusCore contractScript))
                        Haskell.putStrLn    $   "       Plutus Binary Size (Bytes): "   <>  show (format $  scriptSize contractScript)
                        Haskell.putStrLn    $   "       CPU Usage (Step/PSec):      "   <>  show (format $  getCPUExBudget d contractScript)
                        Haskell.putStrLn    $   "       Memory Usage (Bytes):       "   <>  show (format $  getMemoryExBudget d contractScript)
                        Haskell.putStrLn    $   "       Script Hash:                "   <>  show (scriptHash contractScript)
                        Haskell.putStrLn        "\n<---------------------------------------------------------------------------------------------->\n"

                        --putStrLn        "\n<---------------------------------UNTYPED PLUTUS CORE---------------------------------->\n"
                        --print       $       prettyPlcReadableDef uplc
                        --putStrLn          "\n<-------------------------------------------------------------------------------------->\n"
--
                        --putStrLn        "\n<----------------------------------TYPED PLUTUS CORE----------------------------------->\n"
                        --print       $       prettyPlcReadableDef $  contractParams
                        --putStrLn          "\n<-------------------------------------------------------------------------------------->\n"
--
                        --putStrLn        "\n<----------------------------------------PLUTUS IR------------------------------------->\n"
                        --print       $       pretty $ fromJust (Just (pir contractParams))
                        --putStrLn          "\n<-------------------------------------------------------------------------------------->\n"

        -- prettyPIR :: ContractParams -> Doc ann0
        -- prettyPIR cp = pretty $ fromMaybe (error "ERROR: No Plutus IR") (pir cp)

writeMintingAggressiveOptimizedScript  :: CompiledCode (BuiltinData -> BuiltinData -> ()) -> [Data] -> [Char] -> [Char] -> [Char] -> IO ()
writeMintingAggressiveOptimizedScript  cC d contractName conDir coresDir = do
    let validator       =  Plutonomy.optimizeUPLCWith Plutonomy.aggressiveOptimizerOptions $ mkMintingPolicyScript cC
        plc             = getPlc cC
        --pir             = getPir cC
        plcSize         = sizePlc cC
        contractScript  = unMintingPolicyScript validator
    createDirectoryIfMissing True conDir
    createDirectoryIfMissing True $ coresDir </> contractName
    result <- writeCBORValidator (conDir </> contractName <.> ".plutus") contractScript
    case result of
        Left err -> print $ displayError err
        Right () -> do
                        Haskell.writeFile  (coresDir </> contractName </> "prettifiedTypedPlutusCore" <.> "txt") (show $ prettyPlcReadableDef plc)
                        Haskell.writeFile  (coresDir </> contractName </> "rawTypePlutusCore" <.> "txt") (show plc)
                        Haskell.writeFile  (coresDir </> contractName </> "prettifiedUntypedPlutusCore" <.> "txt") (show (prettyPlcReadableDef (unScript contractScript)))
                        Haskell.writeFile  (coresDir </> contractName </> "rawUntypedPlutusCore" <.> "txt") (show (unScript contractScript))
                        --writeFile  (coresDir </> contractName </> contractName <.> ".PIR") (show $ prettyPIR agkp)

                        Haskell.putStrLn        "\n<----------------------------------------CONTRACT INFO----------------------------------------->\n"
                        Haskell.putStrLn    $   "       Name:                       "   ++  contractName
                        Haskell.putStrLn    $   "       Location:                   "   ++  conDir      </> contractName
                        Haskell.putStrLn    $   "       Cores Location:             "   ++  coresDir    </> contractName
                        Haskell.putStrLn    $   "       PLC Size (Bytes):           "   <>  show (format plcSize)
                        Haskell.putStrLn    $   "       CBOR Size (Bytes):          "   <>  show (format $  LBS.length $ writeOnlyJSON contractScript)
                        Haskell.putStrLn    $   "       CBOR Binary Size (Bytes):   "   <>  show (format $  length (plutusCore contractScript))
                        Haskell.putStrLn    $   "       Plutus Binary Size (Bytes): "   <>  show (format $  scriptSize contractScript)
                        Haskell.putStrLn    $   "       CPU Usage (Step/PSec):      "   <>  show (format $  getCPUExBudget d contractScript)
                        Haskell.putStrLn    $   "       Memory Usage (Bytes):       "   <>  show (format $  getMemoryExBudget d contractScript)
                        Haskell.putStrLn    $   "       Script Hash:                "   <>  show (scriptHash contractScript)
                        Haskell.putStrLn        "\n<---------------------------------------------------------------------------------------------->\n"

                        --putStrLn        "\n<---------------------------------UNTYPED PLUTUS CORE---------------------------------->\n"
                        --print       $       prettyPlcReadableDef uplc
                        --putStrLn          "\n<-------------------------------------------------------------------------------------->\n"
--
                        --putStrLn        "\n<----------------------------------TYPED PLUTUS CORE----------------------------------->\n"
                        --print       $       prettyPlcReadableDef $  contractParams
                        --putStrLn          "\n<-------------------------------------------------------------------------------------->\n"
--
                        --putStrLn        "\n<----------------------------------------PLUTUS IR------------------------------------->\n"
                        --print       $       pretty $ fromJust (Just (pir contractParams))
                        --putStrLn          "\n<-------------------------------------------------------------------------------------->\n"

        -- prettyPIR :: ContractParams -> Doc ann0
        -- prettyPIR cp = pretty $ fromMaybe (error "ERROR: No Plutus IR") (pir cp)

writeStakingAggressiveOptimizedScript  :: CompiledCode (BuiltinData -> BuiltinData -> ()) -> [Data] -> [Char] -> [Char] -> [Char] -> IO ()
writeStakingAggressiveOptimizedScript  cC d contractName conDir coresDir = do
    let validator       = Plutonomy.optimizeUPLCWith Plutonomy.aggressiveOptimizerOptions $ mkStakeValidatorScript cC
        plc             = getPlc cC
        --pir             = getPir cC
        plcSize         = sizePlc cC
        contractScript  = unStakeValidatorScript validator
    createDirectoryIfMissing True conDir
    createDirectoryIfMissing True $ coresDir </> contractName
    result <- writeCBORValidator (conDir </> contractName <.> ".plutus") contractScript
    case result of
        Left err -> print $ displayError err
        Right () -> do
                        Haskell.writeFile  (coresDir </> contractName </> "prettifiedTypedPlutusCore" <.> "txt") (show $ prettyPlcReadableDef plc)
                        Haskell.writeFile  (coresDir </> contractName </> "rawTypePlutusCore" <.> "txt") (show plc)
                        Haskell.writeFile  (coresDir </> contractName </> "prettifiedUntypedPlutusCore" <.> "txt") (show (prettyPlcReadableDef (unScript contractScript)))
                        Haskell.writeFile  (coresDir </> contractName </> "rawUntypedPlutusCore" <.> "txt") (show (unScript contractScript))
                        --writeFile  (coresDir </> contractName </> contractName <.> ".PIR") (show $ prettyPIR agkp)

                        Haskell.putStrLn        "\n<----------------------------------------CONTRACT INFO----------------------------------------->\n"
                        Haskell.putStrLn    $   "       Name:                       "   ++  contractName
                        Haskell.putStrLn    $   "       Location:                   "   ++  conDir      </> contractName
                        Haskell.putStrLn    $   "       Cores Location:             "   ++  coresDir    </> contractName
                        Haskell.putStrLn    $   "       PLC Size (Bytes):           "   <>  show (format plcSize)
                        Haskell.putStrLn    $   "       CBOR Size (Bytes):          "   <>  show (format $  LBS.length $ writeOnlyJSON contractScript)
                        Haskell.putStrLn    $   "       CBOR Binary Size (Bytes):   "   <>  show (format $  length (plutusCore contractScript))
                        Haskell.putStrLn    $   "       Plutus Binary Size (Bytes): "   <>  show (format $  scriptSize contractScript)
                        Haskell.putStrLn    $   "       CPU Usage (Step/PSec):      "   <>  show (format $  getCPUExBudget d contractScript)
                        Haskell.putStrLn    $   "       Memory Usage (Bytes):       "   <>  show (format $  getMemoryExBudget d contractScript)
                        Haskell.putStrLn    $   "       Script Hash:                "   <>  show (scriptHash contractScript)
                        Haskell.putStrLn        "\n<---------------------------------------------------------------------------------------------->\n"

                        --putStrLn        "\n<---------------------------------UNTYPED PLUTUS CORE---------------------------------->\n"
                        --print       $       prettyPlcReadableDef uplc
                        --putStrLn          "\n<-------------------------------------------------------------------------------------->\n"
--
                        --putStrLn        "\n<----------------------------------TYPED PLUTUS CORE----------------------------------->\n"
                        --print       $       prettyPlcReadableDef $  contractParams
                        --putStrLn          "\n<-------------------------------------------------------------------------------------->\n"
--
                        --putStrLn        "\n<----------------------------------------PLUTUS IR------------------------------------->\n"
                        --print       $       pretty $ fromJust (Just (pir contractParams))
                        --putStrLn          "\n<-------------------------------------------------------------------------------------->\n"

        -- prettyPIR :: ContractParams -> Doc ann0
        -- prettyPIR cp = pretty $ fromMaybe (error "ERROR: No Plutus IR") (pir cp)
