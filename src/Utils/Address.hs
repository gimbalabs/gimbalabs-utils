module  Utils.Address
            (   tryReadAddress
            )
                where

import           Cardano.Api                 (AsType (AsAddressAny),
                                              SlotNo (..), deserialiseAddress)
import           Cardano.Api.Shelley         (Address (..), AddressAny (..))
import           Cardano.Crypto.Hash.Class   (hashToBytes)
import           Cardano.Ledger.BaseTypes    (CertIx (..), TxIx (..))
import           Cardano.Ledger.Credential   (Credential (KeyHashObj, ScriptHashObj),
                                              Ptr (..), StakeReference (..))
import           Cardano.Ledger.Crypto       (StandardCrypto)
import           Cardano.Ledger.Hashes       (ScriptHash (..))
import           Cardano.Ledger.Keys         (KeyHash (..))
import           Data.Text                   (pack)
import           Plutus.V1.Ledger.Credential (Credential (..),
                                              StakingCredential (..))
import           Plutus.V1.Ledger.Crypto     ()
import           Plutus.V2.Ledger.Api        (Address (..), PubKeyHash (..),
                                              ValidatorHash (..))
import           PlutusTx.Builtins           (toBuiltin)
import           Prelude                     as Haskell (Maybe (..), String,
                                                         fromIntegral, ($))



credentialLedgerToPlutus :: Cardano.Ledger.Credential.Credential a StandardCrypto -> Plutus.V1.Ledger.Credential.Credential
credentialLedgerToPlutus (ScriptHashObj (ScriptHash h)) = ScriptCredential $ ValidatorHash $ toBuiltin $ hashToBytes h
credentialLedgerToPlutus (KeyHashObj (KeyHash h))       = PubKeyCredential $ PubKeyHash $ toBuiltin $ hashToBytes h

stakeReferenceLedgerToPlutus :: StakeReference StandardCrypto -> Maybe StakingCredential
stakeReferenceLedgerToPlutus (StakeRefBase x)                   = Just $ StakingHash $ credentialLedgerToPlutus x
stakeReferenceLedgerToPlutus (StakeRefPtr (Ptr (SlotNo x) (Cardano.Ledger.BaseTypes.TxIx y) (CertIx z))) = Just $ StakingPtr (Haskell.fromIntegral x) (Haskell.fromIntegral y) (Haskell.fromIntegral z)
stakeReferenceLedgerToPlutus StakeRefNull                       = Nothing

tryReadAddress :: Haskell.String -> Maybe Plutus.V2.Ledger.Api.Address
tryReadAddress x = case deserialiseAddress AsAddressAny $ pack x of
    Nothing                                      -> Nothing
    Just (AddressByron _)                        -> Nothing
    Just (AddressShelley (ShelleyAddress _ p s)) -> Just Address
        { addressCredential        = credentialLedgerToPlutus p
        , addressStakingCredential = stakeReferenceLedgerToPlutus s
        }
