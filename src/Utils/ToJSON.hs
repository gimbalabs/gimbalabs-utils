{-# LANGUAGE TypeApplications #-}

module  Utils.ToJSON
            (   writeJSON
            ,   writeOnlyJSON
            ,   prettyPrintJSON
            )
                where

import           Cardano.Api                       (ScriptDataJsonSchema (ScriptDataJsonDetailedSchema),
                                                    scriptDataToJson)
import           Cardano.Api.SerialiseTextEnvelope (TextEnvelopeDescr,
                                                    textEnvelopeToJSON)
import           Cardano.Api.Shelley               (PlutusScript (PlutusScriptSerialised),
                                                    PlutusScriptV2,
                                                    fromPlutusData)
import           Codec.Serialise                   (serialise)
import           Data.Aeson                        (ToJSON, encode)
import           Data.Aeson.Encode.Pretty          (encodePrettyToTextBuilder)
import           Data.ByteString.Lazy              (ByteString, toStrict)
import           Data.ByteString.Lazy              as BSL (writeFile)
import           Data.ByteString.Short             (toShort)
import           Data.Text.Lazy                    (Text)
import           Data.Text.Lazy.Builder            (toLazyText)
import           Plutus.V2.Ledger.Api              (Script)
import           PlutusTx                          (ToData, toData)
import           Prelude                           as Haskell (FilePath, IO,
                                                               Maybe (..), (.))


writeJSON :: ToData a => Haskell.FilePath -> a -> Haskell.IO ()
writeJSON file = BSL.writeFile file . encode . scriptDataToJson ScriptDataJsonDetailedSchema . fromPlutusData . toData

contractDescription :: TextEnvelopeDescr
contractDescription  = "..............."

writeOnlyJSON ::  Script -> ByteString
writeOnlyJSON = textEnvelopeToJSON @(PlutusScript PlutusScriptV2) (Just contractDescription) . PlutusScriptSerialised . toShort . toStrict . serialise

prettyPrintJSON :: ToJSON a => a -> Text
prettyPrintJSON = toLazyText . encodePrettyToTextBuilder
