module  Utils.ExBudget
            (   plutusCore
            ,   scriptExBudget
            ,   getCPUExBudget
            ,   getMemoryExBudget
            )
                where

import           Codec.Serialise                        (serialise)
import           Data.ByteString.Lazy                   (toStrict)
import           Data.ByteString.Short                  (ShortByteString,
                                                         toShort)
import           Plutus.V2.Ledger.Api                   (Data (..), ExBudget,
                                                         ExCPU (..),
                                                         ExMemory (..),
                                                         ProtocolVersion (..),
                                                         Script,
                                                         VerboseMode (Verbose),
                                                         evaluateScriptCounting,
                                                         exBudgetCPU,
                                                         exBudgetMemory,
                                                         mkEvaluationContext)
import           PlutusCore                             (defaultCostModelParams)
import           PlutusCore.Evaluation.Machine.ExMemory (CostingInteger)
import           Prelude                                (Either (..),
                                                         Maybe (..), error,
                                                         otherwise, ($))

valentinePV :: ProtocolVersion
valentinePV =  ProtocolVersion 8 0

-- vasilPV :: ProtocolVersion
-- vasilPV =  ProtocolVersion 7 0

plutusCore :: Script -> ShortByteString
plutusCore scr = toShort $ toStrict $ serialise scr

scriptExBudget :: [Data] -> Script -> ExBudget
scriptExBudget d scr
    |   Just costModel  <-  defaultCostModelParams
    ,   Right model     <-  mkEvaluationContext costModel
    ,   ([], Right exB) <-  evaluateScriptCounting valentinePV Verbose model (plutusCore scr) d = exB
    |   otherwise = error "scriptExBudget Failed"

getCPUExBudget :: [Data] -> Script -> CostingInteger
getCPUExBudget d scr
    |   (ExCPU unit) <- exBudgetCPU (scriptExBudget d scr) = unit
    |   otherwise = 0

getMemoryExBudget :: [Data] -> Script -> CostingInteger
getMemoryExBudget d scr
    |   (ExMemory unit) <- exBudgetMemory (scriptExBudget d scr) = unit
    |   otherwise = 0
